﻿#include <iostream>
#include <iomanip>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <cmath>

using namespace std;

//Big-O = O(n)
void print(vector<int> v, int num)
{
	for (int i=0;i<num;i++)
	{
		cout <<setw(6) << v.at(i) ;
		if(i%5 == 4)
		cout<<endl;
	}
}


//* The number of elements.
//Big-O = O(n)
int generate(vector<int> v, int num)
{
	int x=0;
	for (int i=0;i<num;i++)
	{
		x = x + 1;
	}
	return x;
}

//* The sum of all elements.
//Big-O = O(n)
int sum(vector<int> v, int num)
{
	int sum = 0 ;
	for (int i=0;i<num;i++)
	{
	 	sum += v.at(i) ;
	}
	return sum; 
}

//* The highest value.
//Big-O = O(n)
int highest(vector<int> v, int num)
{
	int highest=0;
	for (int i=0;i<num;i++)
	{
		if (v.at(i)>highest)
		{
			highest = v.at(i);
		}
	}
	return highest;
}

//* The lowest value.
//Big-O = O(n)
int lowest (vector<int> v, int num)
{
	int lowest;
	lowest = v.at(0);
	for (int i = 0; i < num; ++i)
	{
		if(v.at(i) < lowest)
		{
			lowest = v.at(i);
		}
	}
	return lowest;
}

//* The mean value.
//Big-O = O(n)
double mean(vector<int> v, int num)
{
    double mean,sum=0;
    for(int i=0;i<num;i++)
    {
        sum+=v.at(i);
    }
    mean=sum/num;
    return mean;
}

//* The median value.
//Big-O = O(1)
double median(vector<int> v, int num)
{
	int median=0;
	if(num%2 == 0)
	{
		median = ((v.at(num*0.5))+(v.at((num*0.5)+1)))/(2);
	}
	else
	{
		median = v.at(num*0.5);
	}
	return median;
}

//* The mode value.
//Big-O = O(n)
int mode(vector<int> v, int num)
{
	int count=0,max=0,getmode=0;;
	int mode,temp=v.at(0);
	for(int i=0;i<num;i++)
	{
		if (v.at(i)==temp)
		{
			count = count + 1;
		}
		else
		{
			if(count > max)
			{
				max = count;
				mode = temp;
				getmode = 1; 
			}
			else if (count == max)
			{
				getmode = 0;
			}
			count = 1;		 
		}
		temp = v.at(i);
	}

	if (getmode == 1)
	{
		return mode;
	}
	else 
	{
		return -1;
	}
}

//* The standard deviation.
//Big-O = O(n)
double standard(vector<int> v, int num)
{
	int x=0;
	int sum=0;
	for(int i=0;i<num;i++)
	{
		x =	v.at(i) - mean(v,num);
		sum = sum + pow(x,2);
	}
	return sqrt(sum / num - 1);
}

//* The number of even numbers.
//Big-O = O(n)
int even(vector<int> v, int num)
{
	int even=0;
	for(int i=0;i<num;i++)
	{
		if(v.at(i) % 2 == 0)
		{
			even = even + 1;
		}
	}
	return even;
}
//* The number of odd numbers.
//Big-O = O(n)
int odd(vector<int> v, int num)
{
	int odd=0;
	for(int i=0;i<num;i++)
	{
		if(v.at(i) % 2 != 0)
		{
			odd = odd + 1;
		}
	}
	return odd;
}

//* The values output in order from lowest to highest.
//Big-O = O(n^2)
void sort(vector<int> &v, int num)
{
	int index,low=0,temp=0;;
	for (int i=0;i<num;i++)
	{
		low = v.at(i);
		for (int j=i;j<num;j++)
		{
			if (v.at(j) < low)
			{
				low = v.at(j);
				index = j;
			}
		}
		temp = v.at(i);
		v.at(i) = v.at(index);
		v.at(index) = temp;
	}
}

//Big-O = O(n^2)
int main()
{
	cout << "**--++Vector and random integers (between 0-100)++--**"<<endl;

	int element=0;
	int num;
	srand(time(0));

	//generate a random number between 50 & 150 
	num = (rand() % 101)+50;


	vector<int> v;

	//Create a vector and general random integers (between 0-100)
	for (int i = 0; i < num; ++i)
	{
		element = rand() % 101;
		v.push_back(element);
	}

	//print vector
	print(v,num);

	cout << endl << endl;

	cout << "The Number of elements: " << generate(v,num) << endl;
	cout << "The Sum of all elements: " << sum(v, num) << endl;
	cout << "The Highest value: " << highest(v, num) << endl;
	cout << "The Lowest value: " << lowest (v, num) << endl;
	cout << "The Mean value: " << mean (v, num) << endl;

	sort(v,num);
	cout << "The Median value: " << median (v, num) << endl;
	cout << "The Mode value: " ;

	if(mode (v, num) == -1)
	{
		cout << "NO MODE!";
	}
	else
	{
		cout << mode (v, num);
	}
	cout << endl;
	cout << "The Standard Deviation: " << standard(v,num) << endl;
	cout << "The Even numbers have : " << even(v,num) << endl;
	cout << "The Odd numbers have: " << odd(v,num) << endl;
	cout << "The values output in order from lowest to highest: " << endl;

	//print
	print(v,num);

	return 0;

}